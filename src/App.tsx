import React, {useEffect, useState} from 'react';
import {Routes, Route} from 'react-router-dom';
import axios from "axios";

import {Button} from './components';
import Home from "./pages/Home";
import EditUser from "./pages/EditUser";


type Filters = {
  type?: string;
  order?: string;
}

function App() {

  const sortButton = [
    {
      name: 'по городу',
      type: 'address.city',
      order: 'asc'
    },
    {
      name: 'по компании',
      type: 'company.name',
      order: 'asc'
    },
  ];

  const [userCards, setUserCards] = useState<object[]>([]);
  const [isLoaded, setIsLoaded] = useState<boolean>(false);

  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/users').then(({data}) => {
      setUserCards(data);
      setIsLoaded(true);
    })
  }, []);


  const filters = ({type, order}: Filters) => {
    setIsLoaded(false);
    axios.get(`https://jsonplaceholder.typicode.com/users?_sort=${type}&_order=${order}`).then(({data}) => {
      setUserCards(data);
      setIsLoaded(true);
    })
  }

  return (
    <div className="wrapper">
      <div className="sidebar">
        <h3 className="sidebar__title">Сортировка</h3>

        {sortButton.map((obg: any, index: number) => (
          <Button
            key={`${obg.name}_${index}`}
            className="mb-10"
            primary
            onClickSort={filters}
            {...obg}
          >
            {obg.name}
          </Button>
        ))}
      </div>
      <div className="main">
        <Routes>
          <Route path="/" element={<Home users={userCards} isLoaded={isLoaded}/>}/>
          <Route path="/edit/:id" element={<EditUser/>}/>
        </Routes>
      </div>
    </div>
  );
}

export default App;
