import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import axios from 'axios';
import {Button} from '../components';


function EditUser() {

  const {id} = useParams();
  const [formValues, setFormValues] = useState<any>([]);
  const [readOnly, setReadOnly] = useState<boolean>(true);
  const [formErrors, setFormErrors] = useState<any>({})
  const [buttonDisabled, setButtonDisabled] = useState<boolean>(true);
  const [isSubmit, setIsSubmit] = useState<boolean>(false);


  useEffect(() => {
    axios.get(`https://jsonplaceholder.typicode.com/users/${id}`).then(({data}) => {
      setFormValues(data);
    })
  }, []);

  useEffect(() => {
    if (Object.keys(formErrors).length === 0 && isSubmit) {
      const sendValues = JSON.stringify(formValues, undefined, 2);
      console.log(sendValues);
    }
  }, [formErrors]);

  const clickReadOnly = () => {
    setReadOnly(false);
    setButtonDisabled(false);
  }


  const handleChange = (e: any) => {
    const {name, value} = e.target;
    const formData = {...formValues};
    if (name === 'street' || name === 'city' || name === 'zipcode') {
      setFormValues({...formData, address: {...formData.address, [name]: value}});
    } else {
      setFormValues({...formData, [name]: value});
    }
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();
    setFormErrors(validate(formValues));
    setIsSubmit(true);
  }

  const validate = (values: any) => {
    const errors: any = {};
    const regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
    const regexNumber = /^\d+(-\d+)*$/;
    if (values.name.length <= 3) {
      errors.name = 'Поле Name должно содержать больше 3 символов';
    }
    if (values.username.length <= 3) {
      errors.username = 'Поле UserName должно содержать больше 3 символов';
    }
    if (!regexEmail.test(values.email)) {
      errors.email = 'Введен не правильниый Email';
    }
    if (values.address.street.length <= 3) {
      errors.street = 'Поле Street должно содержать больше 3 символов';
    }
    if (values.address.city.length <= 3) {
      errors.city = 'Поле City должно содержать больше 3 символов';
    }
    if (values.address.zipcode.length <= 3) {
      errors.zipcode = 'Поле Zipcode должно содержать больше 3 символов';
    } else if (!regexNumber.test(values.address.zipcode)) {
      errors.zipcode = 'Поле Zipcode должно содержать только числа и тире';
    }
    if (values.phone.length <= 3) {
      errors.phone = 'Поле Phone должно содержать больше 3 символов';
    }
    if (values.website.length <= 3) {
      errors.website = 'Поле Website должно содержать больше 3 символов';
    }
    return errors;
  }


  return (
    <div className='main__wrapper'>
      <div className='main__header'>
        <h2 className='main__title'>Профиль пользователя</h2>
        <Button primary onClickReadOnly={clickReadOnly}>Редактировать</Button>
      </div>
      <form className='main__form form' onSubmit={handleSubmit}>
        <div className='form__wrapper'>
          <div className='form__group'>
            <label htmlFor='name'>Name</label>
            <input type='text' id='name' name='name' onChange={handleChange} readOnly={readOnly} required
                   value={formValues.name || ''}/>
            {formErrors.name && <span className='form__errors'>{formErrors.name}</span>}
          </div>
          <div className='form__group'>
            <label htmlFor='userName'>User name</label>
            <input type='text' id='userName' name='username' onChange={handleChange} readOnly={readOnly} required
                   value={formValues.username || ''}/>
            {formErrors.username && <span className='form__errors'>{formErrors.username}</span>}
          </div>
          <div className='form__group'>
            <label htmlFor='email'>Email</label>
            <input type='email' id='email' name='email' onChange={handleChange} required readOnly={readOnly}
                   value={formValues.email || ''}/>
            {formErrors.email && <span className='form__errors'>{formErrors.email}</span>}
          </div>
          <div className='form__group'>
            <label htmlFor='street'>Street</label>
            <input type='text' id='street' name='street' onChange={handleChange} required readOnly={readOnly}
                   value={formValues.address?.street || ''}/>
            {formErrors.street && <span className='form__errors'>{formErrors.street}</span>}
          </div>
          <div className='form__group'>
            <label htmlFor='city'>City</label>
            <input type='text' id='city' name='city' onChange={handleChange} required readOnly={readOnly}
                   value={formValues.address?.city || ''}/>
            {formErrors.city && <span className='form__errors'>{formErrors.city}</span>}
          </div>
          <div className='form__group'>
            <label htmlFor='zipCode'>Zip code</label>
            <input type='text' id='zipCode' name='zipcode' onChange={handleChange} required readOnly={readOnly}
                   value={formValues.address?.zipcode || ''}/>
            {formErrors.zipcode && <span className='form__errors'>{formErrors.zipcode}</span>}
          </div>
          <div className='form__group'>
            <label htmlFor='phone'>Phone</label>
            <input type='text' id='phone' name='phone' onChange={handleChange} required readOnly={readOnly}
                   value={formValues.phone || ''}/>
            {formErrors.phone && <span className='form__errors'>{formErrors.phone}</span>}
          </div>
          <div className='form__group'>
            <label htmlFor='website'>Website</label>
            <input type='text' id='website' name='website' onChange={handleChange} required readOnly={readOnly}
                   value={formValues.website || ''}/>
            {formErrors.website && <span className='form__errors'>{formErrors.website}</span>}
          </div>
          <div className='form__group'>
            <label htmlFor='comment'>Comment</label>
            <textarea id='comment' name='comment' readOnly={readOnly} onChange={handleChange}></textarea>
          </div>
        </div>
        <div className='form__btn'>
          <Button primary success disabled={buttonDisabled}>Отправить</Button>
        </div>
      </form>
    </div>
  );
}

export default EditUser;