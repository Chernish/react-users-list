import React from 'react';
import {UserItem} from "../components";
import LoadingBlock from "../components/ItemBlock/LoadingBlock";

type Props = {
  users?: any;
  isLoaded?: boolean;
}

function Home({users, isLoaded}: Props) {
  return (
    <div className='main__wrapper'>
      <h2 className="main__title">Список пользователей</h2>
      <div className="main__wrapper">
        {isLoaded ? users.map((obg: any) => (
          <UserItem key={obg.id} {...obg} />
        )) : Array(10).fill(0).map((_, index: number) => <LoadingBlock key={index}/>)}
      </div>
      <p className="main__info">Найдено {Object.keys(users).length} пользователей</p>
    </div>
  );
}

export default Home;