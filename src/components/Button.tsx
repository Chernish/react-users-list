import React from 'react';
import classNames from 'classnames';

type Props = {
  className?: string,
  children?: JSX.Element | JSX.Element[] | string,
  green?: boolean,
  primary?: boolean,
  success?: boolean,
  onClickSort?: any,
  onClickReadOnly?: any,
  type?: any,
  order?: any,
  disabled?: boolean,
}

function Button({children, className, primary, onClickSort, type, order, onClickReadOnly, disabled, success}: Props) {
  const buttonClick = () => {
    if (onClickSort) {
      onClickSort({type, order});
    }
    if (onClickReadOnly) {
      onClickReadOnly();
    }
  }
  return (
    <button disabled={disabled} className={classNames('button', className, {
      'button--primary': primary,
      'button--success': success,
    })}
            onClick={buttonClick}
    >
      {children}
    </button>
  );
}

export default Button;